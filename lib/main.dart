import 'package:covid_app/ui/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:covid_app/ui/styles/theme.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid 19 Tracker',
      //debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "Poppins",
        // set app body background color
        scaffoldBackgroundColor: lightGrey,
        // set primary color
        primaryColor: brandColor,
      ),
      home: HomeScreen(),
    );
  }
}