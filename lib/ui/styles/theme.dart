import "package:flutter/material.dart";

// Colors
const brandColor = Color(0xFFff1166);
const white = Color(0xFFFFFFFF);
const black = Color(0xFF000000);
const lightGrey = Color(0xFFFEFEFE);
const grey = Color(0xFFE5E5E5);
const darkGrey = Color(0xFF777777);
const blue = Color(0xFF338cCD);
const darkBlue =  Color(0xFF11247f);
const darkRed = Color(0xFF800000);
const red = Color(0xFFDC143C);
const green = Color(0xFF42b883);
const darkGreen = Color(0xFF007944);
const yellow = Color(0xffffcd3c);
const darkOrange = Color(0xfff45905);
const orange = Color(0xfff45905);
const purple = Color(0XFF8d12fe);
const purpleLight = Color(0XFFefedf2);
const purpleDark = Color(0xff571cc3);
final shadowColor = Color(0xFFB7B7B7).withOpacity(.16);
final activeShadowColor = Color(0xFF4056C6).withOpacity(.15);

// Fonts
const HeadingTextStyle = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.w600,
);

const SubHeadingTextStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w600,
);

const TitleTextStyle = TextStyle(
  fontSize: 18,
  color: blue,
  fontWeight: FontWeight.bold,
);