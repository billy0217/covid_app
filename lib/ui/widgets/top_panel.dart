import 'package:covid_app/ui/screens/info_screen.dart';
import 'package:covid_app/ui/styles/theme.dart';
import "package:flutter/material.dart";
import 'package:flutter_svg/flutter_svg.dart';

class TopPanel extends StatelessWidget {
  final String textTop;
  final String textBottom;
  final String screen;

  const TopPanel({
    Key key,
    this.textTop,
    this.textBottom,
    this.screen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyClipper(),
      child: Container(
        padding: EdgeInsets.only(left: 40, top: 20, right: 20),
        height: 150,
        width: double.infinity,
        // background style
        decoration: BoxDecoration(
          // set gradient
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              purple,
              darkBlue
            ]
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(0),
            topRight: Radius.circular(0),
            bottomLeft: Radius.circular(0),
            bottomRight: Radius.circular(0)
          ),
          // background image
          image: DecorationImage(
            image: AssetImage("images/virus2.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.centerLeft,
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Align(
            //   alignment: Alignment.topRight,
            //   child: IconButton(
            //     icon: SvgPicture.asset("images/icons/menu.svg"),
            //     color: white,
            //     iconSize: 18.0,
            //     padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
            //     onPressed: () {
            //       switch(screen) {
            //         case "home" : {
            //           Navigator.push(
            //             context,
            //             MaterialPageRoute(builder: (context) => InfoScreen())
            //           );
            //         }
            //         break;
                    
            //         default: { 
            //           Navigator.pop(context);
            //         } 
            //         break;
            //       }
            //     },
            //   ),
            // ),
            SizedBox(height: 10),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  // position 
                  Text(
                    "$textTop",
                    style: HeadingTextStyle.copyWith(
                      color: white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25
                    )
                  ),
                ],
              )
            ),
          ]
        )
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 0);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 0);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}