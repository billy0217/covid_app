import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';

class PreventCard extends StatelessWidget {
  final String image;
  final String title;
  final String text;
  
  const PreventCard({
    Key key,
    this.image,
    this.title,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: SizedBox(
        height:170,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: <Widget>[
            Container(
              height: 170,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 8),
                    blurRadius: 24,
                    color: shadowColor
                  )
                ]
              ),
            ),
            Image.asset(image),
            Positioned(
              left: 130,
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 15
                ),
                
                width: MediaQuery.of(context).size.width - 170,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: TitleTextStyle.copyWith(
                        fontSize: 16,
                        color: black
                      )
                    ),
                    Text(
                      text,
                      style: TextStyle(
                        fontSize: 12, 
                      ),
                    )
                  ],
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}