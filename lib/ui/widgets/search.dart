
import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';
import 'package:number_display/number_display.dart';

class Search extends SearchDelegate {

  final List countryList;

  Search(this.countryList);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton( 
        icon: Icon(Icons.clear),
        onPressed: (){
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(icon: Icon(Icons.arrow_back_ios),onPressed: (){
      Navigator.pop(context);
    },);
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
                          ? countryList
                          : countryList.where(
                              (element) => element['country'].toString().toLowerCase().startsWith(query)
                            ).toList();
    final display = createDisplay(length: 20);
    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context,index){
        return Card(
          child: Container(
            height: 130,
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: <Widget>[
                Container(
                  width: 120,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        suggestionList[index]['country'],
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Image.network(
                        suggestionList[index]['countryInfo']['flag'],
                        height: 40,
                        width: 50,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                      Text(
                        'New cases: ' +  display(suggestionList[index]['todayCases']),
                        style: TextStyle(
                            fontSize: 14,
                            color: orange
                        ),
                      ),
                      Text(
                        'Confirmed: ' +  display(suggestionList[index]['cases']),
                        style: TextStyle(
                            fontSize: 14,
                            color: darkBlue
                        ),
                      ),
                      Text(
                        'Active: ' + display(suggestionList[index]['active']),
                        style: TextStyle(
                          fontSize: 14,
                          color: red
                        ),
                      ),
                      Text(
                        'Recovered: ' + display(suggestionList[index]['recovered']),
                        style: TextStyle(
                            fontSize: 14,
                            color: green
                          ),
                      ),
                      Text(
                        'Deathers: ' +  display(suggestionList[index]['deaths']),
                        style: TextStyle(
                          fontSize: 14,
                          color: black
                        ),
                      ),
                    ],
                    ),
                  )
                )
              ],
            ),
          )
        );
      }
    );
  }  
}