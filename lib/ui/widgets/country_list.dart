import 'dart:async';
import 'dart:convert';
import 'package:covid_app/core/models/Debouncer.dart';
import 'package:covid_app/core/models/SharedPref.dart';
import 'package:covid_app/core/models/User.dart';
import 'package:covid_app/core/viewmodels/CasesInfo.dart';
import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:number_display/number_display.dart';
import 'package:intl/intl.dart' as intl;
import 'package:shared_preferences/shared_preferences.dart';

class CountryList extends StatefulWidget {
  CountryList({Key key}) : super(key: key);

  @override
  _CountryListState createState() => _CountryListState();
}

class _CountryListState extends State<CountryList> {
  SharedPref sharedPref = SharedPref();
  List<CasesInfo> cases = List();
  List<CasesInfo> filteredCoutries = List();
  //static final int _itemLength = snapshot.data.documents[index]['title'].length;
  
  final _debouncer = Debouncer(milliseconds: 500);
  //bool _isFavorited = false;
  List _isFavorited;
  List favoritCountries = [];
  List favoritCountryIndex = [];
  User userSave = User();
  User userLoad = User();

  loadSharedPrefs() async {
    
    if(await sharedPref.read("user") != null){
      User user = User.fromJson(await sharedPref.read("user"));
      // Scaffold.of(context).showSnackBar(SnackBar(
      //     content: new Text("Loaded!"),
      //     duration: const Duration(milliseconds: 500)));
      setState(() {
        userLoad = user;
        favoritCountries = user.data;
      });
    }
    
  }

  @override
  void initState() {
    super.initState();
    //prefsFavoritCountries();
    Services.getData().then((casesFromServer) {
      setState(() {
        cases = casesFromServer;
        filteredCoutries = cases;
        _isFavorited = List.generate(cases.length, (_) => false);
        
        for(var i = 0; i < cases.length; i++){
          if(favoritCountries.contains(filteredCoutries[i].country)){
            print(filteredCoutries[i].country);
            filteredCoutries[i].favorit = true;
            print(filteredCoutries[i].favorit);
            favoritCountryIndex.add(i);
          }
        }
        for ( var i in favoritCountryIndex ){
          _isFavorited[i] = true;
        }
        // print(_isFavorited);
        // print(favoritCountryIndex);
      });
    });
    loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    final display = createDisplay(length: 20);
    // print('favoritCountries');
    // print(favoritCountries);
    // print('userLoad.data');
    // print(userLoad.data);
    // print(userSave.data);
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 10
            ),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(50.0),
                  ),
                ),
                contentPadding: EdgeInsets.all(10.0),
                prefixIcon: Icon(
                  Icons.search,
                  color: darkBlue,
                ),
                hintText: 'Filter by country name',
                focusColor: purpleDark,
                focusedBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(50.0),
                  ),
                  borderSide: BorderSide(color: darkBlue),
                ),
              ),
              autofocus: false,
              onChanged: (string) {
                _debouncer.run(() {
                  setState(() {
                    filteredCoutries = cases
                        .where((c) => (c.country
                                .toLowerCase()
                                .contains(string.toLowerCase()) ))
                        .toList();
                  });
                });
              },
            ),
          ),
          SizedBox(
            height: 10
          ),
          filteredCoutries == null ? CircularProgressIndicator()
          : ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.all(10.0),
              itemCount: filteredCoutries.length,
              itemBuilder: (context, index) {
                return Card(
                  child: InkWell(
                    highlightColor: white,
                    hoverColor: white,
                    splashColor: white,
                    onTap: (){
                      setState(() {
                        //print(_isFavorited[index]);
                        //_isFavorited[index] = !_isFavorited[index];
                        filteredCoutries[index].favorit = !filteredCoutries[index].favorit;
                        //print(filteredCoutries[index].country);
                        //print('set State');
                        //print(favoritCountries.toSet());

                        if(filteredCoutries[index].favorit){
                          if(!favoritCountries.contains(filteredCoutries[index].country)){
                            favoritCountries.add(filteredCoutries[index].country.toString());
                          }
                        }else{
                          if(favoritCountries.contains(filteredCoutries[index].country)){
                            favoritCountries.remove(filteredCoutries[index].country.toString());
                          }
                        }

                        // print('on tap');
                        //print(filteredCoutries[index].favorit);
                        
                        // print(filteredCoutries[index].country);
                        userSave.data = favoritCountries;
                        // print('save on tap');
                        // print(userSave.data);
                        sharedPref.save("user", userSave);
                        
                      });
                      
                    },
                    child: Container(
                      //height: 170,
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 110,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  filteredCoutries[index].country,
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                Image.network(
                                  filteredCoutries[index].countryInfo,
                                  height: 40,
                                  width: 50,
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Text(
                                  'New cases: ' +  display(filteredCoutries[index].todayCases),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: orange
                                  ),
                                ),
                                Text(
                                  'Active: ' + display(filteredCoutries[index].active),
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: red
                                  ),
                                ),
                                Text(
                                  'Recovered: ' + display(filteredCoutries[index].recovered),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: green
                                    ),
                                ),
                                Text(
                                  'Confirmed: ' +  display(filteredCoutries[index].cases),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: darkBlue
                                  ),
                                ),
                                Text(
                                  'Deaths: ' +  display(filteredCoutries[index].deaths),
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: black
                                  ),
                                ),
                              ],
                              ),
                            )
                          ),
                          Icon(
                            filteredCoutries[index].favorit ? Icons.star : Icons.star_border,
                            color: filteredCoutries[index].favorit ? yellow.withOpacity(0.9) : darkGrey.withOpacity(0.5),
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
        ]
      ),
    );
  }
}