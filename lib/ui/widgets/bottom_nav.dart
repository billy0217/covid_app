import 'package:covid_app/ui/screens/countries_screen.dart';
import 'package:covid_app/ui/screens/home_screen.dart';
import 'package:covid_app/ui/screens/info_screen.dart';
import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';

class BottomNavBar extends StatelessWidget {
  final bool isHome;
  final bool isInfo;
  final bool isList;
  const BottomNavBar({
    Key key,
    this.isHome = false,
    this.isInfo = false, 
    this.isList = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      color: purpleDark,
      child: Container(
        height: 60,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox.fromSize(
              size: Size(60, 60), // button width and height
              child: ClipOval(
                child: Material(
                  color: purpleDark, // button color
                  child: InkWell(
                    splashColor: purple, // splash color
                    onTap: () {
                      if(isHome){
                        return null;
                      }else{
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => HomeScreen())
                        );
                      }
                    }, // button pressed
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.home,
                          color: white,
                          size: 25,
                        ),
                        SizedBox(height:5), // icon
                        Text(
                          "Home",
                          style: TextStyle(
                            fontSize: 10,
                            height: 1,
                            color: white,
                          ),
                        ), 
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox.fromSize(
              size: Size(60, 60), // button width and height
              child: ClipOval(
                child: Material(
                  color: purpleDark, // button color
                  child: InkWell(
                    splashColor: purple, // splash color
                    onTap: () {
                      if(isList){
                        return null;
                      }else{
                        Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CountriesScreen())
                      );
                      }
                    }, // button pressed
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.language,
                          color: white,
                          size: 25,
                        ),
                        SizedBox(height:5), // icon
                        Text(
                          "Worldwide",
                          style: TextStyle(
                            fontSize: 10,
                            height: 1,
                            color: white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox.fromSize(
              size: Size(60, 60), 
              child: ClipOval(
                child: Material(
                  color: purpleDark, 
                  child: InkWell(
                    splashColor: purple, 
                    onTap: () {
                      if(isInfo){
                        return null;
                      }else{
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => InfoScreen())
                        );
                      }
                    }, 
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.info_outline,
                          color: white,
                          size: 25,
                        ),
                        SizedBox(height:5), 
                        Text(
                          "Info",
                          style: TextStyle(
                            fontSize: 10,
                            height: 1,
                            color: white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}