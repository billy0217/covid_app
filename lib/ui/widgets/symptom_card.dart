import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';

class SymptomCard extends StatelessWidget {
  final String image;
  final String title;
  final bool isActive;
  
  const SymptomCard({
    Key key, 
    this.image,
    this.title,
    this.isActive = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: white,
        boxShadow: [
          isActive
            ? BoxShadow(
                offset: Offset(0, 10),
                blurRadius: 20,
                color: activeShadowColor
              )
            : BoxShadow(
              offset: Offset(0, 3),
              blurRadius: 6,
              color: shadowColor
            ),
        ],
      ),
      child: Column(
        children: <Widget>[
          Image.asset(image, height: 60),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold
            ),
          )
        ],
      ),
    );
  }
}