import 'package:covid_app/ui/styles/theme.dart';
import 'package:flutter/material.dart';
import 'package:number_display/number_display.dart';

class Counter extends StatelessWidget {
  final int number;
  final Color color;
  final Color panelColor;
  final String title;
  
  const Counter({
    Key key, 
    this.number,
    this.color,
    this.panelColor,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final display = createDisplay(length: 20);
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 5
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: panelColor.withOpacity(0.35),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 4),
                blurRadius: 30,
                color: grey
              ),
            ]
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /*Container(
                padding: EdgeInsets.all(6),
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: color.withOpacity(0.26)
                ),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.transparent,
                    border: Border.all(
                      color: color,
                      width: 2,
                    )
                  )
                )
              ),
              SizedBox(width:10),*/
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: display(number),
                      style: TitleTextStyle.copyWith(
                        fontSize: 20,
                        height: 1.3,
                        color: color
                      ),
                    ),
                    TextSpan(
                      text: "\n$title",
                      style: TextStyle(
                        color: color,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ]
          ),
        )
      ]
    );
  }
}