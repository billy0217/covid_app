import 'package:covid_app/ui/screens/info_screen.dart';
import 'package:covid_app/ui/styles/theme.dart';
import "package:flutter/material.dart";
import 'package:flutter_svg/flutter_svg.dart';

class TopBanner extends StatelessWidget {
  final String image;
  final String textTop;
  final String textBottom;
  final String screen;

  const TopBanner({
    Key key,
    this.image,
    this.textTop,
    this.textBottom,
    this.screen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyClipper(),
      child: Container(
        padding: EdgeInsets.only(left: 40, top: 50, right: 20),
        height: 350,
        width: double.infinity,
        // background style
        decoration: BoxDecoration(
          // set gradient
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              blue,
              darkBlue
            ]
          ),
          // background image
          image: DecorationImage(
            image: AssetImage("images/virus.png")
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: SvgPicture.asset("images/icons/menu.svg"),
                color: white,
                iconSize: 18.0,
                padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                onPressed: () {
                  switch(screen) {
                    case "home" : {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => InfoScreen())
                      );
                    }
                    break;
                    
                    default: { 
                      Navigator.pop(context);
                    } 
                    break;
                  }
                },
              ),
            ),
            SizedBox(height: 20),
            Expanded(
              child: Stack(
                children: <Widget>[
                  SvgPicture.asset(
                    image,
                    width: 230,
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.topCenter,
                  ),
                  // position 
                  Positioned(
                    top: 20,
                    left: 150,
                    child: Text(
                      "$textTop \n$textBottom",
                      style: HeadingTextStyle.copyWith(
                        color: white
                      )
                    ),
                  ),
                  Container(),
                ],
              )
            )
          ]
        )
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}