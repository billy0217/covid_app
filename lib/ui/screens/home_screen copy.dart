import 'package:covid_app/ui/styles/theme.dart';
import 'package:covid_app/ui/widgets/bottom_nav.dart';
import 'package:covid_app/ui/widgets/counter.dart';
import 'package:covid_app/ui/widgets/top_panel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:covid_app/ui/screens/countries_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // clip mask
            TopPanel(
              textTop: 'Covid-19 Tracker',
              screen: "home",
            ),
            
            SizedBox(height:20),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(height:20),
                  Row(
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Case Update\n",
                              style: TitleTextStyle.copyWith(
                                color: darkBlue,
                              ),
                            ),
                            TextSpan(
                              text: "Newest update March 28",
                              style: TextStyle(
                                color: darkGrey,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) {
                                    return CountriesScreen();
                                  },
                                ),
                              );
                            },
                            child: Text(
                              "See all countries",
                              style: TextStyle(
                                color: brandColor,
                                fontWeight: FontWeight.w600
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height:20),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 0
                    ),
                    padding: EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 20
                    ),
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(
                        color: grey
                      )
                    ),
                    child: Row(
                      children: <Widget>[
                        SvgPicture.asset("images/icons/maps-and-flags.svg"),
                        SizedBox(width:20),
                        Expanded(
                          child: DropdownButton(
                            isExpanded: true,
                            underline: SizedBox(),
                            icon: SvgPicture.asset("images/icons/dropdown.svg"),
                            value: 'New Zealand',
                            items: <String>['New Zealand', 'Korea', 'United States']
                              .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (value){}),
                          )
                      ]
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Wrap(
                          direction: Axis.horizontal,
                          spacing: 5.0,
                          runSpacing: 0.0,
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: Counter(
                                color: orange,
                                panelColor: yellow,
                                number: 1500,
                                title: "New Cases"
                              ),
                            ),
                            SizedBox(
                              width:  ( MediaQuery.of(context).size.width / 2  - 22.5),
                              child: Counter(
                                color: darkRed,
                                panelColor: red,
                                number: 15003,
                                title: "Active Cases"
                              ),
                            ),
                            SizedBox(
                              width:  ( MediaQuery.of(context).size.width / 2  - 22.5),
                              child: Counter(
                                color: darkBlue,
                                panelColor: blue,
                                number: 1488,
                                title: "Total Confirmed"
                              ),
                            ),
                            SizedBox(
                              width:  ( MediaQuery.of(context).size.width / 2  - 22.5),
                              child: Counter(
                                color: darkRed,
                                panelColor: red,
                                number: 15003,
                                title: "Active Cases"
                              ),
                            ),
                            SizedBox(
                              width:  ( MediaQuery.of(context).size.width / 2  - 22.5),
                              child: Counter(
                                color: darkRed,
                                panelColor: red,
                                number: 15003,
                                title: "Active Cases"
                              ),
                            ),
                          ]
                        ),
                        
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                flex: 6,
                                child: Counter(
                                  color: darkRed,
                                  panelColor: red,
                                  number: 15003,
                                  title: "Active Cases"
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Counter(
                                  color: darkBlue,
                                  panelColor: blue,
                                  number: 1488,
                                  title: "Total Confirmed"
                                ),
                              ),
                            ]
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                flex: 6,
                                child: Counter(
                                  color: darkGreen,
                                  panelColor: green,
                                  number: 15003,
                                  title: "Total Recovered"
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Counter(
                                  color: black,
                                  panelColor: black,
                                  number: 1488,
                                  title: "Total Deaths"
                                ),
                              ),
                            ]
                          ),
                        ),
                        SizedBox(height:50)
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
    ),
      ),
      bottomNavigationBar: BottomNavBar(
        isHome: true,
      ),
    );
  }
}