import 'dart:convert';

import 'package:covid_app/core/models/User.dart';
import 'package:covid_app/ui/styles/theme.dart';
import 'package:covid_app/ui/widgets/bottom_nav.dart';
import 'package:covid_app/ui/widgets/counter.dart';
import 'package:covid_app/ui/widgets/top_panel.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:covid_app/ui/screens/countries_screen.dart';
import 'package:intl/intl.dart' as intl;
import 'package:covid_app/core/models/SharedPref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SharedPref sharedPref = SharedPref();
  Map countryData;
  User userLoad = User();
  List favoritCountryList = [];
  String dropdownValue;

  fetchCountryData(counrty) async {
    var url;
    //print(counrty);
    if(counrty == ''){
      url = 'https://corona.lmao.ninja/v2/countries/New Zealand';
    }else{
      url = 'https://corona.lmao.ninja/v2/countries/'+counrty;
    }
    http.Response response = await http.get(url);
    
    print('fetch data : '+ counrty);

    setState(() {
      countryData = json.decode(response.body);
    });
  }

  Future fetchData(counrty) async {
    fetchCountryData(counrty);
    print('fetchData called');
  }

  loadSharedPrefs() async {
    if(await sharedPref.read("user") != null){
      User user = User.fromJson(await sharedPref.read("user"));

      if(user != null){
        favoritCountryList = user.data;
        dropdownValue = favoritCountryList[0].toString();
      }
    }
    
    //print(favoritCountryList[0].toString());
  }

  @override
  void initState() {
    fetchData('');
    super.initState();
    loadSharedPrefs();
  }

  
  var updateDate;

  @override
  Widget build(BuildContext context) {
    updateDate = countryData == null? '' : intl.DateFormat('E dd/MM/yyyy').format(DateTime.fromMillisecondsSinceEpoch(countryData['updated'], isUtc: true));
    
    print(DateTime.now().timeZoneName);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // clip mask
            TopPanel(
              textTop: 'Covid-19 Tracker',
              screen: "home",
            ),

            SizedBox(height: 20),
            countryData == null
                ? CircularProgressIndicator()
                : Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return CountriesScreen();
                                    },
                                  ),
                                );
                              },
                              child: Text(
                                "See all countries",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: brandColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Case Update\n",
                                    style: TitleTextStyle.copyWith(
                                      color: darkBlue,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "Last updated on " +
                                        updateDate.toString(),
                                    style: TextStyle(
                                      color: darkGrey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Spacer(),
                          ],
                        ),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 0),
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: white,
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: grey)),
                          child: Row(children: <Widget>[
                            SvgPicture.asset("images/icons/maps-and-flags.svg"),
                            SizedBox(width: 20),
                            Expanded(
                              child: DropdownButton<String>(
                                  isExpanded: true,
                                  underline: SizedBox(),
                                  icon: SvgPicture.asset(
                                      "images/icons/dropdown.svg"),
                                  value: dropdownValue,
                                  items: favoritCountryList.map((dynamic value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    setState(() {
                                      dropdownValue = newValue;
                                      print(newValue);
                                      fetchData(newValue);
                                    });
                                  }),
                            )
                          ]),
                        ),
                        SizedBox(height: 20),
                        favoritCountryList.isEmpty ?
                          Text(
                            'Please select your favourit country from Worldwide screen',
                          ):
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Wrap(
                                    direction: Axis.horizontal,
                                    spacing: 5.0,
                                    runSpacing: 0.0,
                                    children: [
                                      SizedBox(
                                        width: double.infinity,
                                        child: Counter(
                                            color: orange,
                                            panelColor: yellow,
                                            number: countryData['todayCases'],
                                            title: "New Cases"),
                                      ),
                                      SizedBox(
                                        width:
                                            (MediaQuery.of(context).size.width /
                                                    2 -
                                                22.5),
                                        child: Counter(
                                            color: darkRed,
                                            panelColor: red,
                                            number: countryData['active'],
                                            title: "Active Cases"),
                                      ),
                                      SizedBox(
                                        width:
                                            (MediaQuery.of(context).size.width /
                                                    2 -
                                                22.5),
                                        child: Counter(
                                            color: darkBlue,
                                            panelColor: blue,
                                            number: countryData['cases'],
                                            title: "Total Confirmed"),
                                      ),
                                      SizedBox(
                                        width:
                                            (MediaQuery.of(context).size.width /
                                                    2 -
                                                22.5),
                                        child: Counter(
                                            color: darkGreen,
                                            panelColor: green,
                                            number: countryData['recovered'],
                                            title: "Total Recovered"),
                                      ),
                                      SizedBox(
                                        width:
                                            (MediaQuery.of(context).size.width /
                                                    2 -
                                                22.5),
                                        child: Counter(
                                            color: black,
                                            panelColor: black,
                                            number: countryData['deaths'],
                                            title: "Total Deaths"),
                                      ),
                                    ]),
                                SizedBox(height: 50)
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavBar(
        isHome: true,
      ),
    );
  }
}
