import 'dart:async';
import 'dart:convert';
import 'package:covid_app/core/models/Debouncer.dart';
import 'package:covid_app/ui/styles/theme.dart';
import 'package:covid_app/ui/widgets/bottom_nav.dart';
import 'package:covid_app/ui/widgets/country_list.dart';
import 'package:covid_app/ui/widgets/top_panel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:number_display/number_display.dart';
import 'package:intl/intl.dart' as intl;


class CountriesScreen extends StatefulWidget { 
  @override
  _CountriesScreenState createState() => _CountriesScreenState();
}


class _CountriesScreenState extends State<CountriesScreen> {
  final _debouncer = Debouncer(milliseconds: 500);
  Map worldData;
  // fetch API data
  fetcgWordWideData()async{
    // get request from API 
    http.Response response = await http.get('https://corona.lmao.ninja/v2/all');

    setState(() {
      worldData = json.decode(response.body);
    });
  }

  List countryData;
  List filteredCountries = List();

  fetchCountryData() async {
    http.Response response = await http.get('https://corona.lmao.ninja/v2/countries?sort=cases');

    setState(() {
      countryData = json.decode(response.body);
      filteredCountries = countryData;
    });
  }

  Future fetchData() async{
    fetcgWordWideData();
    fetchCountryData();
    print('fetchData called');
  }

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  var updateDate;
  
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
        bottomNavigationBar: BottomNavBar(
          isHome: false,
          isList: true,
        ),
        body: RefreshIndicator(
          onRefresh: fetchData,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TopPanel(
                  textTop: "Worldwide update",
                  screen: "info",
                ),
                SizedBox(height:20),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CountryList(),
                      worldData == null
                        ? CircularProgressIndicator()
                        :  WorldwidePanel(worldData: worldData),
                      countryData == null
                        ? CircularProgressIndicator()
                        : CountryData(countryData: countryData),

                    ]
                  )
                )
              ]
            )
          ),
        )
      );
  }
}



class WorldwidePanel extends StatelessWidget {
  final Map worldData;
  
  const WorldwidePanel({Key key, this.worldData}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    
    final updateDate = worldData == null? '' : intl.DateFormat('yMEd').format(DateTime.fromMillisecondsSinceEpoch(worldData['updated'], isUtc: true));
    
    return Container(
      padding: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Last updated: "+ updateDate,
              style: TitleTextStyle.copyWith(
              color: black
            ),
            textAlign: TextAlign.left,
          ),
          GridView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, 
              childAspectRatio: 2
            ),
            children: <Widget>[
              StatusPanel(
                panelColor: blue,
                textColor: darkBlue,
                title: "Total Confirmed",
                number: worldData["cases"],
              ),
              StatusPanel(
                panelColor: red,
                textColor: darkRed,
                title: "Total Active",
                number: worldData["active"],
              ),
              StatusPanel(
                panelColor: green,
                textColor: darkGreen,
                title: "Total Recovered",
                number: worldData["recovered"],
              ),
              StatusPanel(
                panelColor: black,
                textColor: black,
                title: "Total Deaths",
                number: worldData["deaths"],
              ),
            ],
          ),
        ],
      )
    );
  }
}

class StatusPanel extends StatelessWidget {

  final Color panelColor;
  final Color textColor;
  final String title;
  final int number;

  const StatusPanel({
    Key key,
    this.panelColor,
    this.textColor,
    this.title,
    this.number
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    // update number format
    final display = createDisplay(length: 20);
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: 80,
      width: width/2,
      margin: EdgeInsets.fromLTRB(5, 0, 5, 10),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: panelColor.withOpacity(0.35),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 5,
              color: grey
            ),
          ]
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              display(number),
              style: SubHeadingTextStyle.copyWith(
                fontSize: 20,
                color: textColor,
                height: 1.5,
              )
            ),
            Text(
              "$title",
              style: TextStyle(
                fontSize: 14,
                color: textColor,
                height: 1.3,
              ),
            )
          ],
        ),
      ),
    );
  }
}


class CountryData extends StatelessWidget {
  final List countryData;

  const CountryData({Key key, this.countryData}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    
    final TextEditingController editingController = TextEditingController();
    final display = createDisplay(length: 20);
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              onChanged: (value) {
                print(value);
                
              },
              controller: editingController,
              decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25.0))
                )
              ),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Card(
                child: Container(
                  height: 130,
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 120,
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              countryData[index]['country'],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Image.network(
                              countryData[index]['countryInfo']['flag'],
                              height: 40,
                              width: 50,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                            Text(
                              'New cases: ' +  display(countryData[index]['todayCases']),
                              style: TextStyle(
                                  fontSize: 14,
                                  color: orange
                              ),
                            ),
                            Text(
                              'Active: ' + display(countryData[index]['active']),
                              style: TextStyle(
                                fontSize: 14,
                                color: red
                              ),
                            ),
                            Text(
                              'Recovered: ' + display(countryData[index]['recovered']),
                              style: TextStyle(
                                  fontSize: 14,
                                  color: green
                                ),
                            ),
                            Text(
                              'Confirmed: ' +  display(countryData[index]['cases']),
                              style: TextStyle(
                                  fontSize: 14,
                                  color: darkBlue
                              ),
                            ),
                            Text(
                              'Deathers: ' +  display(countryData[index]['deaths']),
                              style: TextStyle(
                                fontSize: 14,
                                color: black
                              ),
                            ),
                          ],
                          ),
                        )
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: countryData == null ? 0 : countryData.length,
          ),
        ]
      ),
    );
  }
}