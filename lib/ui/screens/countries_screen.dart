import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:covid_app/core/models/Debouncer.dart';
import 'package:covid_app/ui/styles/theme.dart';
import 'package:covid_app/ui/widgets/bottom_nav.dart';
import 'package:covid_app/ui/widgets/country_list.dart';
import 'package:covid_app/ui/widgets/top_panel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:number_display/number_display.dart';
import 'package:intl/intl.dart' as intl;


class CountriesScreen extends StatefulWidget { 
  @override
  _CountriesScreenState createState() => _CountriesScreenState();
}


class _CountriesScreenState extends State<CountriesScreen> {
  final _debouncer = Debouncer(milliseconds: 500);
  Map worldData;
  // fetch API data
  fetcgWordWideData()async{
    // get request from API 
    http.Response response = await http.get('https://corona.lmao.ninja/v2/all');

    setState(() {
      worldData = json.decode(response.body);
    });
  }

  List countryData;
  List filteredCountries = List();

  fetchCountryData() async {
    http.Response response = await http.get('https://corona.lmao.ninja/v2/countries?sort=cases');

    setState(() {
      countryData = json.decode(response.body);
      filteredCountries = countryData;
    });
  }

  Future fetchData() async{
    fetcgWordWideData();
    fetchCountryData();
    print('fetchData called');
  }

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  var updateDate;
  
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
        bottomNavigationBar: BottomNavBar(
          isHome: false,
          isList: true,
        ),
        body: RefreshIndicator(
          onRefresh: fetchData,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TopPanel(
                  textTop: "Worldwide update",
                  screen: "info",
                ),
                SizedBox(height:20),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      worldData == null
                        ? CircularProgressIndicator()
                        :  WorldwidePanel(worldData: worldData),
                      SizedBox(
                        height: 30
                      ),
                      CountryList()
                    ]
                  )
                )
              ]
            )
          ),
        )
      );
  }
}



class WorldwidePanel extends StatelessWidget {
  final Map worldData;
  
  const WorldwidePanel({Key key, this.worldData}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    //print(ui.window.locale);
    //print(DateTime.now().timeZoneName);
    
    //final timezome = ui.window.locale;
    final updateDate = worldData == null? '' : intl.DateFormat('E dd/MM/yyyy').format(DateTime.fromMillisecondsSinceEpoch(worldData['updated'], isUtc: true));
    
    return Container(
      padding: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Last updated: "+ updateDate,
              style: TitleTextStyle.copyWith(
              color: black
            ),
            textAlign: TextAlign.left,
          ),
          SizedBox(
            height: 20
          ),
          Wrap(
            direction: Axis.horizontal,
            spacing: 5.0,
            runSpacing: 0.0,
            children: [
              StatusPanel(
                panelColor: blue,
                textColor: darkBlue,
                title: "Total Confirmed",
                number: worldData["cases"],
              ),
              StatusPanel(
                panelColor: red,
                textColor: darkRed,
                title: "Total Active",
                number: worldData["active"],
              ),
              StatusPanel(
                panelColor: green,
                textColor: darkGreen,
                title: "Total Recovered",
                number: worldData["recovered"],
              ),
              StatusPanel(
                panelColor: black,
                textColor: black,
                title: "Total Deaths",
                number: worldData["deaths"],
              ),
            ],
          ),
        ],
      )
    );
  }
}

class StatusPanel extends StatelessWidget {

  final Color panelColor;
  final Color textColor;
  final String title;
  final int number;

  const StatusPanel({
    Key key,
    this.panelColor,
    this.textColor,
    this.title,
    this.number
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    // update number format
    final display = createDisplay(length: 20);
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: 80,
      width: width/2 - 32.5,
      margin: EdgeInsets.fromLTRB(5, 0, 5, 10),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: panelColor.withOpacity(0.35),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 5,
              color: grey
            ),
          ]
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              display(number),
              style: SubHeadingTextStyle.copyWith(
                fontSize: 20,
                color: textColor,
                height: 1.5,
              )
            ),
            Text(
              "$title",
              style: TextStyle(
                fontSize: 14,
                color: textColor,
                height: 1.3,
              ),
            )
          ],
        ),
      ),
    );
  }
}