import 'package:covid_app/ui/styles/theme.dart';
import 'package:covid_app/ui/widgets/bottom_nav.dart';
import 'package:covid_app/ui/widgets/prevent_card.dart';
import 'package:covid_app/ui/widgets/symptom_card.dart';
import 'package:covid_app/ui/widgets/top_panel.dart';
import 'package:flutter/material.dart';

class InfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBar(
        isHome: false,
        isInfo: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TopPanel(
              textTop: "Get to Know",
              textBottom: "About Covid-19",
              screen: "info",
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height:20),
                  Text(
                    "Most Common Symptoms",
                    style: TitleTextStyle.copyWith(
                      color: black
                    )
                  ),
                  SizedBox(height:20),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        fit: FlexFit.tight,
                        flex: 100,
                        child: SymptomCard(
                          image: "images/light_cough.gif",
                          title: "Cough",
                          isActive: true,
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        flex: 100,
                        child: SymptomCard(
                          image: "images/light_fever.gif",
                          title: "Fever",
                          isActive: true,
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        flex: 100,
                        child: SymptomCard(
                          image: "images/light_tiredness.gif",
                          title: "Tiredness",
                          isActive: true,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height:20),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 5
                    ),
                    child: Text(
                      "Prevention",
                      style: TitleTextStyle.copyWith(
                        color: black,
                      ),
                    ),
                  ),
                  SizedBox(height:20),
                  PreventCard(
                    image: "images/wash_hands.png",
                    title: "Wash your hand",
                    text: "Washing your hands with soap and water or using alcohol-based hand rub kills viruses that may be on your hands.",
                  ),
                  PreventCard(
                    image: "images/wear_mask.png",
                    title: "Avoid touching eyes, nose and mouth",
                    text: "Hands touch many surfaces and can pick up viruses, hands can transfer the virus to your eyes, nose or mouth.",
                  ),
                  PreventCard(
                    image: "images/wear_mask.png",
                    title: "Wear face mask",
                    text: "Avoiding contact with others will protect them from possible COVID-19 and other viruses",
                  ),
                  PreventCard(
                    image: "images/wear_mask.png",
                    title: "If you’re sick, stay home",
                    text: "If you have a fever, cough and difficulty breathing, seek medical attention. Call in advance.",
                  ),
                  SizedBox(height: 50)
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}

