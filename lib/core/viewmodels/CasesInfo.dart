import 'dart:convert';
import 'package:http/http.dart' as http;

class CasesInfo {
  String country;
  String countryInfo;
  int cases;
  int todayCases;
  int deaths;
  int recovered;
  int active;
  bool favorit;

  CasesInfo({
    this.country,
    this.countryInfo,
    this.cases,
    this.todayCases,
    this.deaths,
    this.recovered,
    this.active,
    this.favorit
  });

  factory CasesInfo.fromJson(Map<String, dynamic> json) {
    return CasesInfo(
      country: json["country"] as String,
      countryInfo: json["countryInfo"]["flag"] as String,
      cases: json["cases"] as int,
      todayCases: json["todayCases"] as int,
      deaths: json["deaths"] as int,
      recovered: json["recovered"] as int,
      active: json["active"] as int,
      favorit: false
    );
  }
}


class Services {
  static const String url = 'https://corona.lmao.ninja/v2/countries?sort=cases';

  static Future<List<CasesInfo>> getData() async {
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        List<CasesInfo> list = parseData(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<CasesInfo> parseData(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<CasesInfo>((json) => CasesInfo.fromJson(json)).toList();
  }
}